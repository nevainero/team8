import { observer }  from 'mobx-react-lite'
import store         from '../../store'
import { useEffect } from 'react'
import './table.css'

export default observer(() => {
    useEffect(() => store.loadData(), [])
    const { data } = store.state
    const { uploadData, clear } = store

    const loader = <div className='lds-ring'>
      <div />
      <div />
      <div />
      <div />
    </div>

    return (
      <div className='container'>
        <div className='row mb-5'>
          <div className='col'>
            <button type='button' className='btn btn-success'
              onClick={uploadData}>
              Выгрузить на МойСклад
            </button>
            <button type='button' className='btn btn-danger' onClick={clear}>
              Очистить таблицу
            </button>
          </div>
        </div>

        <div className='row'>
          <div className='col'>
            <table className='table ' style={{ width: '100%' }}>
              <thead>
              <tr>
                <th scope='col'>Наименование</th>
                <th scope='col'>Производитель</th>
                <th scope='col'>Цена розничная</th>
                <th scope='col'>Цена оптовая</th>
                <th scope='col'>Количество</th>
                <th scope='col'>Наличие</th>
              </tr>
              </thead>
              <tbody>
              {data.length ? data.map((el, i) => {
                const { name, brand, price, optPrice, count, available } = el
                return (
                  <tr key={i}>
                    <th scope='col'>{name || '-'}</th>
                    <th scope='col'>{brand || '-'}</th>
                    <th scope='col'>{price || '-'}</th>
                    <th scope='col'>{optPrice || '-'}</th>
                    <th scope='col'>{count || '-'}</th>
                    <th scope='col'>{available || '-'}</th>
                  </tr>
                )
              }) : loader}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
)