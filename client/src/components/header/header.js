import { Nav, Navbar } from 'react-bootstrap'
import { observer }    from 'mobx-react-lite'

export default observer(() => {
    return (
      <Navbar className='p-3 mb-5' bg='primary' variant='dark' expand='lg'>
        <Navbar.Brand href='#home'>Team 8</Navbar.Brand>
        <Navbar.Toggle aria-controls='basic-navbar-nav' />
        <Navbar.Collapse id='basic-navbar-nav'>
          <Nav className='mr-auto'>
            <Nav.Link>Таблица</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
)
