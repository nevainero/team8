const xlsx = require('xlsx')
const fs = require('fs')

const parse1 = require('./parsers/parser1')
const parse2 = require('./parsers/parser2')
const parse3 = require('./parsers/parser3')

function readFile (fileName) {
  return xlsx.readFile(fileName, {
    raw: false,
    cellText: false,
    celHTML: false,
    cellStyles: false,
    cellDates: true
  })
}

async function writeFile (outputFile, object) {
  const wbSheet = JSON.stringify(object)
  fs.writeFile(outputFile, wbSheet, 'utf8', (err) => {
    if (err) throw err
  })
}

function getData () {
  const filename1 = '../data/table1.xls'
  const filename2 = '../data/table2.xls'
  const filename3 = '../data/table3.xls'
  const filename4 = '../data/table4.xlsx'

  const data1 = readFile(filename1)
  const data2 = readFile(filename2)
  const data3 = readFile(filename3)
  const data4 = readFile(filename4)

  const parsed1 = parse1(data1)
  const parsed2 = parse1(data2)
  const parsed3 = parse2(data3)
  const parsed4 = parse3(data4)

  const parsed = [...parsed1, ...parsed2, ...parsed3, ...parsed4]
  writeFile('data/result.json', parsed)
  return parsed
}

module.exports = getData