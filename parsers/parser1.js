module.exports =  function parse(data) {
    let products = [];
    const columnInfo = {
        name: 'A',
        brand: 'E',
        optPrice: 'G',
        price: 'H',
        available: 'I'
    }

    const sheetNames = data.SheetNames;
    const workingSheet = data.Sheets[sheetNames[0]];

    const startRow = 15;
    const endRow = findLastRowInSheet(workingSheet);
    // TODO: проверять на пустую строку
    let count = 0;
    for (let i = startRow; i < endRow; i++) {
        // если строка похожа на шаблон (1000*1200), то пропускаем ее
        let sizeOfProduct;
        if (/^[0-9]{1,4}/.test(workingSheet[columnInfo.name + i].v)) {
            sizeOfProduct = workingSheet[columnInfo.name + i].v;
            continue;
        }
        let product = {};
        product.name = getCellValue(workingSheet, columnInfo.name, i) + (sizeOfProduct ? ' ' + sizeOfProduct : '');
        product.brand = getCellValue(workingSheet, columnInfo.brand, i);
        // workingSheet[columnInfo.brand + i].v;
        product.price = getCellValue(workingSheet, columnInfo.price, i);

        product.optPrice = getCellValue(workingSheet, columnInfo.optPrice, i);
        product.count = null;

        product.available = getCellValue(workingSheet, columnInfo.available, i);

        products.push(product);
    }

    return products;
}

function findLastRowInSheet(workingSheet) {
    const rangeInSheet = workingSheet["!ref"];
    const lastRow = rangeInSheet
        .split(':')[1]   // последняя ячейка
        .split('')
        .filter(el => /[0-9]/.test(el))   // отделяем буквы (адрес колонки) от чисел
        .join('');
    
    return lastRow;
}

function getCellValue(workingSheet, columnAddress, rowAddress) {
    let cellValue = workingSheet[columnAddress + '' + rowAddress];

    // если в ячейке есть значение
    if (typeof cellValue != undefined && !!cellValue) {
        // возвращаем значение, избавляясь от лишних пробелов
        return (cellValue.v + '').trim().split(/\s+/).join(' ');
    }
    else {
        return null;
    }

}