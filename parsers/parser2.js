module.exports =  function parse(data) {
    let products = [];
    const columnInfo = {
        name: 'A',
        count: 'E'
    }

    const sheetNames = data.SheetNames;
    const workingSheet = data.Sheets[sheetNames[0]];

    const startRow = findFirstProductsRow(workingSheet);
    const endRow = findLastRowInSheet(workingSheet);
    
    // let count = 0;
    for (let i = startRow; i < endRow; i++) {
        // если строка похожа на шаблон (1000*1200),nameто пропускаем ее
        if (!isEmptyRow(workingSheet, i)) {
            product = {};
            product.name = workingSheet[columnInfo.name + i].v;
            product.count = workingSheet[columnInfo.count + i].v;

            products.push(product);
        }
    }

    return products;
}

function isEmptyRow(workingSheet, count) {
    return typeof workingSheet['A' + count] === 'undefined' || Boolean(workingSheet['A' + count].v.trim()) === false;
}

/**
 *  Фунция нахождения первой значимой строки. Работает специально для этого шаблона
 **/
function findFirstProductsRow(workingSheet, startSearchFromRowNumber = 5) {
    // пропускаем несколько первых строк
    count = startSearchFromRowNumber;
    while (typeof workingSheet['A' + count] === 'undefined' || workingSheet['A' + count].v.split(/\s+/).length <= 1) {
        count++;
    }
    return count;
}

function findLastRowInSheet(workingSheet) {
    const rangeInSheet = workingSheet["!ref"];
    const lastRow = rangeInSheet
        .split(':')[1]   // последняя ячейка
        .split('')
        .filter(el => /[0-9]/.test(el))   // отделяем буквы (адрес колонки) от чисел
        .join('');
    
    return lastRow;
}

function getCellValue(workingSheet, stringAddress, columnAddress) {
    const docMerges = workingSheet['!merges'];
    let cellValue = workingSheet[numberToCharAddress(stringAddress) + '' + (columnAddress.r + 1)];

    // если в ячейке есть значение
    if (typeof cellValue != undefined && !!cellValue) {
        // возвращаем значение, избавляясь от лишних пробелов
        return (cellValue.v + '').trim().split(/\s+/).join(' ');
    }
    else {
        return null;
    }

}